const express = require('express')
const app = express()
const port = 3000

const jsonBody = require("body/json");
var scores = [{ name: "Edwin", score: 50 }, { name: "David", score: 39 }];
let resources = {
    "/scores": JSON.stringify(scores)
};

app.use(express.static('public'))

app.get('/scores', function (req, res) {
    // res.send('Hello World!')
    if (resources[req.url] === undefined) {
        res.statusCode = 404;
        res.end("ERROR NOT FOUND");
    } else {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/javascript');
        const responseBody = resources[req.url];
        res.end(responseBody);
    }
})

app.post('/scores', function (req, res) {
    res.statusCode = 201;
    res.setHeader('Content-Type', 'application/javascript');
    jsonBody(req, res, function (err, body) {
        scores.push(body)
        scores.sort(function (a, b) {
            return b.score - a.score
        })
        scores.splice(3)
        resources["/scores"] = JSON.stringify(scores)
        res.end()
    })
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))